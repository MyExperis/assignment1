﻿using Assignment1.Attributes;
using Assignment1.characters;
using Assignment1.items;
using System;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new Warrior("Jane Doe");
            Weapon axe = new Weapon()
            {
                Name = "Common axe",
                Level = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            warrior.EquipItem(axe);

            Armor plateBody = new Armor()
            {
                Name = "Common plate body armor",
                Level = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorPrimaryAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            warrior.EquipItem(plateBody);
            warrior.PrintCharacterStats();
        }
    }
}
