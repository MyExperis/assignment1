﻿using Assignment1.Expections;
using Assignment1.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.characters
{
    public abstract class Character
    {
        #region Declaring the class variables
        public string Name { get; set; }
        public int Level { get; set; }
        public int Damage { get; set; }
        public List<Armor> charArmor { get; set; }
        public List<Weapon> charWeapon { get; set; }
        private PrimaryAttributes totalPrimaryAttributes;
        private SecondaryAttributes secondaryAttributes = new SecondaryAttributes();
        public List<WeaponTypes> AllowedWeapons { get; set; }
        public List<ArmorTypes> AllowedArmors { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; } = new PrimaryAttributes();
        public PrimaryAttributes TotalPrimaryAttributes { get 
            {
                totalPrimaryAttributes = BasePrimaryAttributes;
                foreach (Item item in CharacterEquipment.Values)
                {
                    if (item is Armor)
                    {
                        Armor tempArmor = (Armor)item;
                        totalPrimaryAttributes += tempArmor.ArmorPrimaryAttributes;
                    }
                }
                return totalPrimaryAttributes;
            }
        } 
        public SecondaryAttributes SecondaryAttributes { get 
            {
                secondaryAttributes.Health = TotalPrimaryAttributes.Vitality * 10;
                secondaryAttributes.ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity;
                secondaryAttributes.ElementalResistance = TotalPrimaryAttributes.Intelligence;

                return secondaryAttributes;
            }
            set
            {
                secondaryAttributes = value;
            } 
        }
       

        public Dictionary<ItemSlot, Item> CharacterEquipment { get; set; } = new Dictionary<ItemSlot, Item>
        {
            {ItemSlot.Body, null },
            {ItemSlot.Head, null },
            {ItemSlot.Legs, null },
            {ItemSlot.Weapon, null }
        };

        #endregion

        /// <summary>
        /// Constructor that initiates the character with its name and initial level wich is 1 for all classes.
        /// </summary>
        /// <param name="charName">the characters name</param>
        public  Character(string charName)
        {
            Name = charName;
            Level = 1;
            Damage = 1;
        }



        /// <summary>
        /// Sets the new individual subclass attributes when leveling up.
        /// </summary>
        public abstract void CharLevelUp();

        /// <summary>
        /// Instantiates the base attributes for each subclass.
        /// </summary>
        protected abstract void InitBaseAttributes();

        /// <summary>
        /// Sets the allowed armor and weapon for the subclass.
        /// </summary>
        protected abstract void SetAllowedItems();

        /// <summary>
        /// Takes an int and levels the character up in regards to the specific subclass's increment in stats.
        /// </summary>
        /// <param name="level">int, amount of levels to level up</param>
        public void LevelUp(int level)
        {
            if (level < 1)
            {
                throw new ArgumentException("Invalid input");
            } else
            {
                Level += level;
                for (int i = 0; i < level; i++)
                {
                    CharLevelUp();
                }
            }
        }

        private string EquipArmor(Armor armor)
        {
        
            if (!AllowedArmors.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException($"This character cant use {armor.ArmorType} as armor");
            } else if (Level < armor.Level)
            {
                throw new InvalidArmorException($"The { armor.Name }'s level is too high for this character");
            } 
            CharacterEquipment[armor.ItemSlot] = armor;
            return $"New armor equipped, made by {armor.ArmorType}!";
        }

        private string EquipWeapon(Weapon weapon)
        {
            if (!AllowedWeapons.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException($"This character cant use {weapon.WeaponType} as a weapon");
            } else if (Level < weapon.Level)
            {
                throw new InvalidWeaponException($"The { weapon.Name }'s level is too high for this character");
            }   
            CharacterEquipment[weapon.ItemSlot] = weapon;
            return $"New weapon equipped, a {weapon.WeaponType}!";
        }

        /// <summary>
        /// Calls CalculateCharDamage from the subclass with it's main attribute to base the damage calculation on.
        /// </summary>
        /// <returns>double, damage</returns>
        public abstract double CallCalcDamage(); 

        /// <summary>
        /// Returns a characters damage in form of a double, calculates in the weapons damage if one is equipped.
        /// </summary>
        /// <param name="classAttributeStat">int, subclass's main attribute</param>
        /// <returns>double, damage to subclass</returns>
        public double CalculateCharDamage(int classAttributeStat)
        {
            double resultDamage = Damage;
            if (CharacterEquipment[ItemSlot.Weapon] != null)
            {
                Weapon tempWeapon = (Weapon)CharacterEquipment[ItemSlot.Weapon];
                resultDamage = (tempWeapon.WeaponAttributes.Damage  * tempWeapon.WeaponAttributes.AttackSpeed) * (1 + classAttributeStat / 100);
            }

            return resultDamage;
        }


        /// <summary>
        /// Equips either an armor or weapon item if it's not too high level or not compatible with the specific character class. Returns a string with result or throws InvalidArmorException or InvalidWeaponException.
        /// </summary>
        /// <param name="item">Item, weapon or armor to be equipped</param>
        /// <returns>string, returns the result of equipped item</returns>
        public string EquipItem(Item item)
        {
            string res = "";
            if (item is Armor)
            {
                if (Level <= item.Level)
                {
                    res = EquipArmor(item as Armor);
                } else
                {
                    throw new InvalidArmorException($"The {item.Name}'s level is too high for this character");
                }
            } else if (item is Weapon) {
                if (Level <= item.Level)
                {
                    res = EquipWeapon(item as Weapon);
                } else
                {
                    throw new InvalidWeaponException($"The {item.Name}'s level is too high for this character");
                }
                
            }

            return res;
            
        }

        /// <summary>
        /// Formatted StringBuilder to print the characters name and stats.
        /// </summary>
        public void PrintCharacterStats()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"Character name: {Name}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character level: {Level}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character strength: {TotalPrimaryAttributes.Strength}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character dexterity: {TotalPrimaryAttributes.Dexterity}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character intelligence: {TotalPrimaryAttributes.Intelligence}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character health: {SecondaryAttributes.Health}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character armor rating: {SecondaryAttributes.ArmorRating}");
            stringBuilder.AppendLine();
            stringBuilder.Append($"Character elemental resistance: {SecondaryAttributes.ElementalResistance}");
            stringBuilder.AppendLine();
            double damage = CallCalcDamage();
            stringBuilder.Append($"Character damage: {damage}");
            Console.WriteLine(stringBuilder);
        }
    }
}
