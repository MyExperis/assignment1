﻿using Assignment1.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.characters
{
    public class Mage : Character
    {

        /// <summary>
        /// Constructor to initiate the bse constructor with it's name and instantiate the allowed items and armor for the class.
        /// </summary>
        /// <param name="CharName">The characters name</param>
        public Mage (string CharName): base(CharName)
        {
            InitBaseAttributes();
            SetAllowedItems();
        }

        /// <summary>
        /// Calls the parents damage calculation in regards to the class's main attribute to base the damage on.
        /// </summary>
        /// <returns>double, the characters damage</returns>
        public override double CallCalcDamage()
        {
            return CalculateCharDamage(TotalPrimaryAttributes.Intelligence);
        }

        protected override void InitBaseAttributes()
        {
            BasePrimaryAttributes.Vitality = 5;
            BasePrimaryAttributes.Strength = 1;
            BasePrimaryAttributes.Dexterity = 1;
            BasePrimaryAttributes.Intelligence = 8;

        }

        /// <summary>
        /// Adds to the characters attributes when leveling up
        /// </summary>
        public override void CharLevelUp()
        {
            BasePrimaryAttributes += new PrimaryAttributes()
            {
                Vitality = 3,
                Strength = 1,
                Dexterity = 1,
                Intelligence = 5
            };
        }

        protected override void SetAllowedItems()
        {
            AllowedWeapons = new List<WeaponTypes> { WeaponTypes.Staff, WeaponTypes.Wand };
            AllowedArmors = new List<ArmorTypes> { ArmorTypes.Cloth };
        }
    }
}
