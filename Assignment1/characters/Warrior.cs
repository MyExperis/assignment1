﻿using Assignment1.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.characters
{
    public class Warrior : Character
    {
        /// <summary>
        /// Constructor to initiate the bse constructor with it's name and instantiate the allowed items and armor for the class.
        /// </summary>
        /// <param name="CharName">The characters name</param>
        public Warrior(string CharName) : base(CharName)
        {
            InitBaseAttributes();
            SetAllowedItems();
        }

        protected override void SetAllowedItems()
        {
            AllowedWeapons = new List<WeaponTypes> { WeaponTypes.Axe, WeaponTypes.Hammer, WeaponTypes.Sword };
            AllowedArmors = new List<ArmorTypes> { ArmorTypes.Mail, ArmorTypes.Plate };
        }

        /// <summary>
        /// Calls the parents damage calculation in regards to the class's main attribute to base the damage on.
        /// </summary>
        /// <returns>double, the characters damage</returns>
        public override double CallCalcDamage()
        {
            return CalculateCharDamage(TotalPrimaryAttributes.Strength);
        }


        protected override void InitBaseAttributes()
        {
            BasePrimaryAttributes.Vitality = 10;
            BasePrimaryAttributes.Strength = 5;
            BasePrimaryAttributes.Dexterity = 2;
            BasePrimaryAttributes.Intelligence = 1;
            Level = 1;

        }

        /// <summary>
        /// Adds to the characters attributes when leveling up
        /// </summary>
        public override void CharLevelUp()
        {
            BasePrimaryAttributes += new PrimaryAttributes()
            {
                Vitality = 5,
                Strength = 3,
                Dexterity = 2,
                Intelligence = 1
            };
        }
    }
}
