﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.items
{
    public abstract class Item
    {
        public String Name { get; set; }
        public int Level { get; set; }
        public ItemSlot ItemSlot { get; set; }

    }
}
