﻿using Assignment1.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.items
{
    public class Weapon : Item
    {
        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponTypes WeaponType { get; set; }

    }
}
