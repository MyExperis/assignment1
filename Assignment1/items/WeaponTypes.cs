﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.items
{
    public enum WeaponTypes
    {
        Axe, 
        Bow, 
        Dagger, 
        Hammer, 
        Staff, 
        Sword, 
        Wand
    }
}
