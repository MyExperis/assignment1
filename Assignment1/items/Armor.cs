﻿using Assignment1.characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1.items
{
    public class Armor : Item

    {
        public ArmorTypes ArmorType { get; set; }
        public PrimaryAttributes ArmorPrimaryAttributes { get; set; }

        public Armor ()
        {
            
        }

    }
}
