﻿namespace Assignment1.characters
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public static PrimaryAttributes operator +(PrimaryAttributes primAttr1, PrimaryAttributes primAttr2)
        {
            PrimaryAttributes combinedPrimAttr = new PrimaryAttributes()
            {
                Strength = primAttr1.Strength + primAttr2.Strength,
                Vitality = primAttr1.Vitality + primAttr2.Vitality,
                Dexterity = primAttr1.Dexterity + primAttr2.Dexterity,
                Intelligence = primAttr1.Intelligence + primAttr2.Intelligence
            };

            return combinedPrimAttr;
        }
    }
}