﻿using Assignment1.characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestAssignment1.TestCharacter
{
    public class TestChar
    {
        [Fact]
        public void Level_InitialLevelCheck_MatchExpected()
        {
            //Arrange and act
            Warrior testWarrior = new Warrior("Jane Doe");
            int expected = 1;
            //Assert
            Assert.Equal(expected, testWarrior.Level);
        }

        [Fact]
        public void Level_LevelUp_MatchExpected()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            int expected = 2;
            int testLevel = 1;
            //Act
            testWarrior.LevelUp(testLevel);
            //Assert
            Assert.Equal(expected, testWarrior.Level);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Level_InvalidLevel_ThrowsArgumen(int input)
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            //Act and assert
            Assert.Throws<ArgumentException>(() => testWarrior.LevelUp(input));
        }
      
    }
}
