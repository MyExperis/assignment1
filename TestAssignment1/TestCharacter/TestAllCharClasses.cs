﻿using Assignment1.characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestAssignment1.TestCharacter
{
    public class TestAllCharClasses
    {
        [Fact]
        public void TotalPrimaryAttributes_DefaultWarrior_MatchExpected()
        {
            //Arrange and act
            Warrior testWarrior = new Warrior("Jane Doe");
            List<int> expected = new() { 10, 5, 2, 1 };
            List<int> actual = new List<int>
            {
                testWarrior.TotalPrimaryAttributes.Vitality,
                testWarrior.TotalPrimaryAttributes.Strength,
                testWarrior.TotalPrimaryAttributes.Dexterity,
                testWarrior.TotalPrimaryAttributes.Intelligence
            };
            //Act
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalPrimaryAttributes_DefaultMage_MatchExpected()
        {
            //Arrange and act
            Mage testMage = new Mage("Jane Doe");
            List<int> expected = new() { 5, 1, 1, 8 };
            List<int> actual = new List<int>
            {
                testMage.TotalPrimaryAttributes.Vitality,
                testMage.TotalPrimaryAttributes.Strength,
                testMage.TotalPrimaryAttributes.Dexterity,
                testMage.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalPrimaryAttributes_DefaultRanger_MatchExpected()
        {
            //Arrange and act
            Ranger testRanger = new Ranger("Jane Doe");
            List<int> expected = new() { 8, 1, 7, 1 };
            List<int> actual = new List<int>
            {
                testRanger.TotalPrimaryAttributes.Vitality,
                testRanger.TotalPrimaryAttributes.Strength,
                testRanger.TotalPrimaryAttributes.Dexterity,
                testRanger.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalPrimaryAttributes_DefaultRogue_MatchExpected()
        {
            //Arrange and act
            Rogue testRogue = new Rogue("Jane Doe");
            List<int> expected = new() { 8, 2, 6, 1 };
            List<int> actual = new List<int>
            {
                testRogue.TotalPrimaryAttributes.Vitality,
                testRogue.TotalPrimaryAttributes.Strength,
                testRogue.TotalPrimaryAttributes.Dexterity,
                testRogue.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);
        }

        //-----------------------------------------------------------------

        [Fact]
        public void TotalPrimaryAttributes_LevelUpAttributesWarrior_MatchExpected()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            //Act
            testWarrior.LevelUp(1);
            List<int> expected = new() { 15, 8, 4, 2 };
            List<int> actual = new List<int>
            {
                testWarrior.TotalPrimaryAttributes.Vitality,
                testWarrior.TotalPrimaryAttributes.Strength,
                testWarrior.TotalPrimaryAttributes.Dexterity,
                testWarrior.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void TotalPrimaryAttributes_LevelUpAttributesMage_MatchExpected()
        {
            //Arrange and act
            Mage testMage = new Mage("Jane Doe");
            testMage.LevelUp(1);
            List<int> expected = new() { 8, 2, 2, 13 };
            List<int> actual = new List<int>
            {
                testMage.TotalPrimaryAttributes.Vitality,
                testMage.TotalPrimaryAttributes.Strength,
                testMage.TotalPrimaryAttributes.Dexterity,
                testMage.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void TotalPrimaryAttributes_LevelUpAttributesRanger_MatchExpected()
        {
            //Arrange and act
            Ranger testRanger = new Ranger("Jane Doe");
            testRanger.LevelUp(1);
            List<int> expected = new() { 10, 2, 12, 2 };
            List<int> actual = new List<int>
            {
                testRanger.TotalPrimaryAttributes.Vitality,
                testRanger.TotalPrimaryAttributes.Strength,
                testRanger.TotalPrimaryAttributes.Dexterity,
                testRanger.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalPrimaryAttributes_LevelUpAttributesRogue_MatchExpected()
        {
            //Arrange and act
            Rogue testRogue = new Rogue("Jane Doe");
            testRogue.LevelUp(1);
            List<int> expected = new() { 11, 3, 10, 2 };
            List<int> actual = new List<int>
            {
                testRogue.TotalPrimaryAttributes.Vitality,
                testRogue.TotalPrimaryAttributes.Strength,
                testRogue.TotalPrimaryAttributes.Dexterity,
                testRogue.TotalPrimaryAttributes.Intelligence
            };
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SecondaryAttributes_LevelUpSecondaryAttributesWarrior_MatchExpected()
        {
            //Arrange and act
            Warrior testWarrior = new Warrior("Jane Doe");
            testWarrior.LevelUp(1);
            List<int> expected = new() { 150, 12, 2 };
            List<int> actual = new List<int>
            {
                testWarrior.SecondaryAttributes.Health,
                testWarrior.SecondaryAttributes.ArmorRating,
                testWarrior.SecondaryAttributes.ElementalResistance
            };
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
