﻿using Assignment1.Attributes;
using Assignment1.characters;
using Assignment1.Expections;
using Assignment1.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestAssignment1.TestItemAndArmor
{
    public class TestArmor
    {
        [Fact]
        public void EquipItem_InvalidArmor_ThrowsException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            Armor testClothHead = new Armor()
            {
                Name = "Common cloth head armor",
                Level = 1,
                ItemSlot = ItemSlot.Head,
                ArmorType = ArmorTypes.Cloth,
                ArmorPrimaryAttributes = new PrimaryAttributes() { Vitality = 1, Intelligence = 5 }
            };
            //Act and assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipItem(testClothHead));

        }



        [Fact]
        public void EquipItem_ValidArmor_SuccessedEquip()
        {
            //Arrange
            string expected = "New armor equipped, made by Plate!";
            Warrior testWarrior = new Warrior("Jane Doe");
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                Level = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorPrimaryAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            //Act and assert
            Assert.Equal(expected, testWarrior.EquipItem(testPlateBody));
        }



        [Fact]
        public void CallCalcDamage_CalculateDamageWeaponAndArmor_MatchExcepted()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                Level = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            testWarrior.EquipItem(testAxe);

            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                Level = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorPrimaryAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            testWarrior.EquipItem(testPlateBody);

            //Act and assert
            Assert.Equal(expected, testWarrior.CallCalcDamage());

        }

        [Fact]
        public void EquipItem_HighLvlArmor_ThrowsException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                Level = 2,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorPrimaryAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            //Act and assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipItem(testPlateBody));

        }
    }
}
