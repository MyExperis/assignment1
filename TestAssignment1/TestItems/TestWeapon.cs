﻿using Assignment1.Attributes;
using Assignment1.characters;
using Assignment1.Expections;
using Assignment1.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestAssignment1.TestItemAndArmor
{
    public class TestWeapon
    {
        [Fact]
        public void CallCalcDamage_NoWeapon_MatchExpected()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            double expected = 1 * (1 + (5 / 100));
            //Act and assert
            Assert.Equal(expected, testWarrior.CallCalcDamage());
        }

        [Fact]
        public void CallCalcDamage_EquippedWeapon_MatchExpected()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            double expected = (7 * 1.1) * (1 + (5 / 100));

            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                Level = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            testWarrior.EquipItem(testAxe);
            //Act and assert
            Assert.Equal(expected, testWarrior.CallCalcDamage());
        }

        [Fact]
        public void EquipItem_ValidWeapon_SuccessedEquip()
        {
            //Arrange
            string expected = "New weapon equipped, a Axe!";
            Warrior testWarrior = new Warrior("Jane Doe");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                Level = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            //Act and assert
            Assert.Equal(expected, testWarrior.EquipItem(testAxe));

        }

        [Fact]
        public void EquipItem_InvalidWeaponType_ThrowsException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            Weapon testBow = new Weapon()
            {
                Name = "Common bow",
                Level = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            //Act and assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipItem(testBow));

        }

        [Fact]
        public void EquipItem_HighLvlWeapon_ThrowsException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Jane Doe");
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                Level = 2,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            //Act and assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipItem(testAxe));

        }
    }
}
